import AdviceGenerator from "./components/AdviceGenerator"

const App = () => {
  return (
    <AdviceGenerator />
  )
}

export default App;