import { createGlobalStyle } from "styled-components"

const Styles = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css2?family=Manrope:wght@800&display=swap');

    :root{
    /* COLORS */
    --color-light-cyan: hsl(193, 38%, 86%);
    --color-neon-green:hsl(150, 100%, 66%);
    --color-grayish-blue:hsl(217, 19%, 38%);
    --color-dark-grayish-blue:hsl(217, 19%, 24%);
    --color-dark-blue:hsl(218, 23%, 16%);

    /* WEIGHTS */
    --font-weight-800: 800;
    }

    html{
        box-sizing:border-box;
    }

    *,*::after,*::before{
        box-sizing: inherit;
    }
    
    body {
        margin:0;
        padding:0;
        font-size: 28px;
        font-family: 'Manrope', sans-serif;
    }
`

export default Styles;