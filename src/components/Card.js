import { useEffect, useState } from "react";
import styled from "styled-components"
import axios from "axios";
import DividerDesktop from '../assets/pattern-divider-desktop.svg';
import DividerMobile from '../assets/pattern-divider-mobile.svg';
import Dice from '../assets/icon-dice.svg';

const Card = () => {
    const [advice, setAdvice] = useState("");

    const getAdvice = () => {
        axios.get("https://api.adviceslip.com/advice")
            .then(res => {
                setAdvice(res.data.slip);
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        document.title = "Frontend Mentor | Advice generator app";
        getAdvice();
    }, []);

    if (advice === "") {
        return <div>Loading...</div>
    }

    return (
        <StyledCard>
            <VFlex>
                <Heading>Advice #{advice.id}</Heading>
                <Quote>{advice.advice}</Quote>
                <DividerImage src={window.innerWidth > 768 ? DividerDesktop : DividerMobile} alt={"pattern divider"} />
            </VFlex>
            <Button onClick={() => getAdvice()}>
                <DiceImage src={Dice} alt={"dice"} />
            </Button>
        </StyledCard>
    )
}

export default Card;

const StyledCard = styled.div`
    background-color:var(--color-dark-grayish-blue);
    border-radius:15px;
    margin:16px;
    max-width: 540px;
    padding: 48px;
    position:relative;
    text-align:center;
    width:100%;
`;

const VFlex = styled.div`
    display:flex;
    flex-direction:column;
    gap:24px;
`;

const Heading = styled.h1`
    color:var(--color-neon-green);
    font-size:13px;
    font-weight:var(--font-weight-800);
    letter-spacing:4.9px;
    text-transform: uppercase;
`;

const Quote = styled.p`
    color:var(--color-light-cyan);
    margin:0;
`

const DividerImage = styled.img`
    width:100%;
    margin-bottom:32px;
`;

const DiceImage = styled.img``;

const Button = styled.button`
    background-color:var(--color-neon-green);
    border-radius:50%;
    border:none;
    bottom:-30px;
    cursor: pointer;
    height:64px;
    left: 0;
    margin-left: auto;
    margin-right: auto;
    position: absolute;
    position:absolute;
    right: 0;
    text-align: center;
    width:64px;
    &:hover {
        filter: drop-shadow(0px 0px 10px var(--color-neon-green));
    }
`