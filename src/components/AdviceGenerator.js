import styled from "styled-components";
import Card from "./Card";

const AdviceGenerator = () => {
  return (
    <Container>
      <Card />
    </Container>
  )
}

export default AdviceGenerator;

const Container = styled.div`
  background-color: var(--color-dark-blue);
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;
